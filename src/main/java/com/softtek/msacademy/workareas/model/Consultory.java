package com.softtek.msacademy.workareas.model;

import javax.persistence.*;
import java.time.LocalTime;

@Entity
@Table(name = "consultories")
public class Consultory {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_consultories")
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "estimated_duration")
    private LocalTime estimated_duration;

    public Consultory() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalTime getEstimated_duration() {
        return estimated_duration;
    }

    public void setEstimated_duration(LocalTime estimated_duration) {
        this.estimated_duration = estimated_duration;
    }
}
