package com.softtek.msacademy.workareas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "qx")
public class Qx {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_qx")
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "qx_number_room")
    private int qx_number_room;
    @Column(name = "qx_clasification")
    private String qx_clasification;

    public Qx() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQx_number_room() {
        return qx_number_room;
    }

    public void setQx_number_room(int qx_number_room) {
        this.qx_number_room = qx_number_room;
    }

    public String getQx_clasification() {
        return qx_clasification;
    }

    public void setQx_clasification(String qx_clasification) {
        this.qx_clasification = qx_clasification;
    }
}
