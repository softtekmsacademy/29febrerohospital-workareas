package com.softtek.msacademy.workareas;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@EnableHystrix
public class WorkAreasApplication {

	public static void main(String[] args) {
		SpringApplication.run(WorkAreasApplication.class, args);
	}

}
