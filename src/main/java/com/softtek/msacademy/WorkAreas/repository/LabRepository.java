package com.softtek.msacademy.workareas.repository;

import com.softtek.msacademy.workareas.model.Laboratory;

public interface LabRepository extends org.springframework.data.jpa.repository.JpaRepository <Laboratory, Integer> {

}
