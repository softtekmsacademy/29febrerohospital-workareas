package com.softtek.msacademy.workareas.repository;

import com.softtek.msacademy.workareas.model.Consultory;

public interface ConsultoryRepository extends org.springframework.data.jpa.repository.JpaRepository <Consultory, Integer> {

}
