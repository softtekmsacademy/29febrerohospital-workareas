package com.softtek.msacademy.workareas.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.softtek.msacademy.workareas.model.Laboratory;
import com.softtek.msacademy.workareas.repository.LabRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

@Service
@Transactional
@DefaultProperties(
    threadPoolProperties = {
            @HystrixProperty(name = "coreSize", value = "8"),
            @HystrixProperty(name = "coreSize", value = "10"),
            @HystrixProperty(name = "maxQueueSize", value = "-1")
    },
    commandProperties = {
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "20"),
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "50"),
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
            @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds", value = "10000"),
            @HystrixProperty(name = "metrics.rollingStats.numBuckets", value = "10")
    }
)
public class LaboratoryServiceImpl implements LaboratoryService {

    @Autowired
    private LabRepository labRepository;

    @Override
    @HystrixCommand(fallbackMethod = "createLabBreaker")
    public Laboratory createLaboratory(Laboratory laboratory) {
        return labRepository.save(laboratory);
    }
    public Laboratory createLabBreaker(Laboratory laboratory){
        Laboratory emptyLaboratory = new Laboratory();
        emptyLaboratory.setName("Error adding :( time out request");
        return emptyLaboratory;
    }

    @Override
    @HystrixCommand(fallbackMethod = "updateLabBreaker")
    public Laboratory updateLaboratory(Laboratory laboratory) {
        Optional<Laboratory> laboratoryDb = this.labRepository.findById(laboratory.getId());

        if (laboratoryDb.isPresent()) {
            Laboratory laboratoryUpdate = laboratoryDb.get();
            laboratoryUpdate.setId(laboratory.getId());
            laboratoryUpdate.setName(laboratory.getName());
            labRepository.save(laboratory);
            return laboratoryUpdate;
        } else {
            throw new ResourceNotFoundException("Record not found with id : " + laboratory.getId());
        }
    }
    public Laboratory updateLaboratoryBreaker(Laboratory laboratory){
        Laboratory empty = new Laboratory();
        empty.setName("Failed updating :(, time out");
        return empty;
    }

    @Override
    @HystrixCommand(fallbackMethod = "getLabsBreaker")
    public List<Laboratory> getLaboratories() {
        return this.labRepository.findAll();
    }
    public List<Laboratory> getLabsBreaker(){
        List<Laboratory> laboratoryList = new ArrayList<>();
        Laboratory laboratory = new Laboratory();
        laboratory.setName("Error :( time out request");
        laboratoryList.add(laboratory);
        return laboratoryList;
    }

    @Override
    @HystrixCommand(fallbackMethod = "getLabBreaker")
    public Laboratory getLaboratory(int id) {
        Optional<Laboratory> laboratoryDb = this.labRepository.findById(id);

        if (laboratoryDb.isPresent()) {
            return laboratoryDb.get();
        } else {
            throw new ResourceNotFoundException("Record not found with id : " + id);
        }
    }
    public Laboratory getLabBreaker(int id){
        Laboratory empty = new Laboratory();
        empty.setName("Error getting Laboratory, time out :(");
        return empty;
    }

    @Override
    @HystrixCommand(fallbackMethod = "deleteLabBreaker")
    public ResponseEntity delete(int id) {
        Optional<Laboratory> laboratoryDb = this.labRepository.findById(id);

        if (laboratoryDb.isPresent()) {
            this.labRepository.delete(laboratoryDb.get());
            return ResponseEntity.ok().body("Delated succesfuly");
        } else {
            throw new ResourceNotFoundException("Record not found with id : " + id);
        }
    }
    public ResponseEntity deleteLabBreaker(@PathVariable int id) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(":( Something went wrong, Error time out");
    }

    @Override
    public boolean exists(int id) {
        return labRepository.existsById(id);
    }
}
