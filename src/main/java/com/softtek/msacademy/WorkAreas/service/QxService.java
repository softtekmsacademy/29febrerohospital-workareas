package com.softtek.msacademy.workareas.service;

import java.util.List;

import com.softtek.msacademy.workareas.model.Qx;
import org.springframework.http.ResponseEntity;

public interface QxService {

    boolean exists(int id);
    
    Qx createQx(Qx qx);

    Qx updateQx(Qx area);

    List<Qx> getOperatingRooms();

    Qx getQx(int id);

    ResponseEntity delete(int id);
}
