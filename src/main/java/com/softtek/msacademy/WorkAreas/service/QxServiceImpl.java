package com.softtek.msacademy.workareas.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.softtek.msacademy.workareas.model.Qx;
import com.softtek.msacademy.workareas.repository.QxRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@Transactional
@DefaultProperties(
    threadPoolProperties = {
            @HystrixProperty(name = "coreSize", value = "8"),
            @HystrixProperty(name = "coreSize", value = "10"),
            @HystrixProperty(name = "maxQueueSize", value = "-1")
    },
    commandProperties = {
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "20"),
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "50"),
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
            @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds", value = "10000"),
            @HystrixProperty(name = "metrics.rollingStats.numBuckets", value = "10")
    }
)
public class QxServiceImpl implements QxService {

    @Autowired
    private QxRepository qxRepository;

    @Override
    @HystrixCommand(fallbackMethod = "createQxBreaker")
    public Qx createQx(Qx qx) {
        return qxRepository.save(qx);
    }
    public Qx createQxBreaker(Qx qx){
        Qx emptyQx = new Qx();
        emptyQx.setName("Error adding :( time out request");
        return emptyQx;
    }

    @Override
    @HystrixCommand(fallbackMethod = "updateQxBreaker")
    public Qx updateQx(Qx qx) {
        Optional<Qx> qxDb = this.qxRepository.findById(qx.getId());

        if (qxDb.isPresent()) {
            Qx qxUpdate = qxDb.get();
            qxUpdate.setId(qx.getId());
            qxUpdate.setName(qx.getName());
            qxUpdate.setQx_number_room(qx.getQx_number_room());
            qxUpdate.setQx_clasification(qx.getQx_clasification());
            qxRepository.save(qx);
            return qxUpdate;
        } else {
            throw new ResourceNotFoundException("Record not found with id : " + qx.getId());
        }
    }
    public Qx updateQxBreaker(Qx qx){
        Qx empty = new Qx();
        empty.setName("Failed updating :(, time out");
        return empty;
    }

    @Override
    @HystrixCommand(fallbackMethod = "getConsultoriesBreaker")
    public List<Qx> getOperatingRooms() {
        return this.qxRepository.findAll();
    }
    public List<Qx> getConsultoriesBreaker(){
        List<Qx> qxList = new ArrayList<>();
        Qx qx = new Qx();
        qx.setName("Error :( time out request");
        qxList.add(qx);
        return qxList;
    }

    @Override
    @HystrixCommand(fallbackMethod = "getQxBreaker")
    public Qx getQx(int id) {
        Optional<Qx> qxDb = this.qxRepository.findById(id);

        if (qxDb.isPresent()) {
            return qxDb.get();
        } else {
            throw new ResourceNotFoundException("Record not found with id : " + id);
        }
    }
    public Qx getQxBreaker(int id){
        Qx empty = new Qx();
        empty.setName("Error getting Qx, time out :(");
        return empty;
    }

    @Override
    @HystrixCommand(fallbackMethod = "deleteQxBreaker")
    public ResponseEntity delete(int id) {
        Optional<Qx> qxDb = this.qxRepository.findById(id);

        if (qxDb.isPresent()) {
            this.qxRepository.delete(qxDb.get());
            return ResponseEntity.ok().body("Delated succesfuly");
        } else {
            throw new ResourceNotFoundException("Record not found with id : " + id);
        }
    }
    public ResponseEntity deleteQxBreaker(int id) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(":( Something went wrong, Error time out");
    }

    @Override
    public boolean exists(int id) {
        return qxRepository.existsById(id);
    }

}
