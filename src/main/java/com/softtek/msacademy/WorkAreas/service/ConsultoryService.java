package com.softtek.msacademy.workareas.service;

import java.util.List;

import com.softtek.msacademy.workareas.model.Consultory;
import org.springframework.http.ResponseEntity;

public interface ConsultoryService {

    boolean exists(int id);

    Consultory createConsultory(Consultory consultory);

    Consultory updateConsultory(Consultory consultory);

    List<Consultory> getConsultories();

    Consultory getConsultory(int id);

    ResponseEntity delete(int id);
}
