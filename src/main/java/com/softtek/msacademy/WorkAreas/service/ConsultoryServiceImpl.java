package com.softtek.msacademy.workareas.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.softtek.msacademy.workareas.model.Consultory;
import com.softtek.msacademy.workareas.repository.ConsultoryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@Transactional
@DefaultProperties(
    threadPoolProperties = {
            @HystrixProperty(name = "coreSize", value = "8"),
            @HystrixProperty(name = "coreSize", value = "10"),
            @HystrixProperty(name = "maxQueueSize", value = "-1")
    },
    commandProperties = {
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "20"),
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "50"),
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
            @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds", value = "10000"),
            @HystrixProperty(name = "metrics.rollingStats.numBuckets", value = "10")
    }
)
public class ConsultoryServiceImpl implements ConsultoryService {

    @Autowired
    private ConsultoryRepository consultoryRepository;

    @Override
    @HystrixCommand(fallbackMethod = "createConsultoryBreaker")
    public Consultory createConsultory(Consultory consultory) {
        return consultoryRepository.save(consultory);
    }
    public Consultory createConsultoryBreaker(Consultory consultory){
        Consultory emptyConsultory = new Consultory();
        emptyConsultory.setName("Error adding :( time out request");
        return emptyConsultory;
    }

    @Override
    @HystrixCommand(fallbackMethod = "updateConsultoryBreaker")
    public Consultory updateConsultory(Consultory consultory) {
        Optional <Consultory > consultoryDb = this.consultoryRepository.findById(consultory.getId());

        if (consultoryDb.isPresent()) {
            Consultory consultoryUpdate = consultoryDb.get();
            consultoryUpdate.setId(consultory.getId());
            consultoryUpdate.setName(consultory.getName());
            consultoryUpdate.setEstimated_duration(consultory.getEstimated_duration());
            consultoryRepository.save(consultory);
            return consultoryUpdate;
        } else {
            throw new ResourceNotFoundException("Record not found with id : " + consultory.getId());
        }
    }
    public Consultory updateConsultoryBreaker(Consultory consultory){
        Consultory empty = new Consultory();
        empty.setName("Failed updating :(, time out");
        return empty;
    }

    @Override
    @HystrixCommand(fallbackMethod = "getConsultoriesBreaker")
    public List<Consultory> getConsultories() {
        return this.consultoryRepository.findAll();
    }
    public List<Consultory> getConsultoriesBreaker() {
        List<Consultory> consultoryList = new ArrayList<>();
        Consultory consultory = new Consultory();
        consultory.setName("Error :( time out request");
        consultoryList.add(consultory);
        return consultoryList;
    }

    @Override
    @HystrixCommand(fallbackMethod = "getConsultoryBreaker")
    public Consultory getConsultory(int id) {
        Optional<Consultory> consultoryDb = this.consultoryRepository.findById(id);

        if (consultoryDb.isPresent()) {
            return consultoryDb.get();
        } else {
            throw new ResourceNotFoundException("Record not found with id : " + id);
        }
    }
    public Consultory getConsultoryBreaker(int id){
        Consultory empty = new Consultory();
        empty.setName("Error getting Consultory, time out :(");
        return empty;
    }

    @Override
    @HystrixCommand(fallbackMethod = "deleteConsultoryBreaker")
    public ResponseEntity delete(int id) {
        Optional<Consultory> consultoryDb = this.consultoryRepository.findById(id);

        if (consultoryDb.isPresent()) {
            this.consultoryRepository.delete(consultoryDb.get());
            return ResponseEntity.ok().body("Delated succesfuly");
        } else {
            throw new ResourceNotFoundException("Record not found with id : " + id);
        }

    }
    public ResponseEntity deleteConsultoryBreaker(int id) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(":( Something went wrong, Error time out");
    }

    @Override
    public boolean exists(int id) {
        return consultoryRepository.existsById(id);
    }

}
