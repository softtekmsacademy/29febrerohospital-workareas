package com.softtek.msacademy.workareas.service;

import java.util.List;

import com.softtek.msacademy.workareas.model.Laboratory;
import org.springframework.http.ResponseEntity;

public interface LaboratoryService {

    boolean exists(int id);
    
    Laboratory createLaboratory(Laboratory laboratory);

    Laboratory updateLaboratory(Laboratory laboratory);

    List<Laboratory> getLaboratories();

    Laboratory getLaboratory(int id);

    ResponseEntity delete(int id);
}
