package com.softtek.msacademy.workareas.controller;

import com.softtek.msacademy.workareas.model.Qx;
import com.softtek.msacademy.workareas.service.QxService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class QxController {
    
    @Autowired
    private QxService service;

    @PostMapping("/operating-rooms")
    public ResponseEntity addNewQx(@RequestBody Qx qx){
        if (service.exists(qx.getId())) {
    		return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid record, already exists");
        }else {
        	service.createQx(qx);
            return ResponseEntity.ok().body(qx);
    	}
    }

    @PutMapping("/operating-rooms/{id}")
    public ResponseEntity<Qx> updateProduct(@PathVariable int id, @RequestBody Qx qx) {
        qx.setId(id);
        return ResponseEntity.ok().body(service.updateQx(qx));
    }

    @GetMapping("/operating-rooms")
    public ResponseEntity<List<Qx>> getOperatingRooms() {
        return ResponseEntity.ok().body(service.getOperatingRooms());
    }

    @GetMapping("/operating-rooms/{id}")
    public ResponseEntity<Qx> getProductById(@PathVariable int id) {
        return ResponseEntity.ok().body(service.getQx(id));
    }

    @DeleteMapping("/operating-rooms/{id}")
    public ResponseEntity deleteQx(@PathVariable int id) {
        return service.delete(id);
    }
}
