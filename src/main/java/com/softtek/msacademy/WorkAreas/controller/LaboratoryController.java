package com.softtek.msacademy.workareas.controller;

import com.softtek.msacademy.workareas.model.Laboratory;
import com.softtek.msacademy.workareas.service.LaboratoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class LaboratoryController {

    @Autowired
    private LaboratoryService service;

    @PostMapping("/laboratories")
    public ResponseEntity addNewLab(@RequestBody Laboratory lab){
        if (service.exists(lab.getId())) {
    		return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid record, already exists");
        }else {
        	service.createLaboratory(lab);
            return ResponseEntity.ok().body(lab);
    	}
    }

    @PutMapping("/laboratories/{id}")
    public ResponseEntity<Laboratory> updateProduct(@PathVariable int id, @RequestBody Laboratory lab) {
        lab.setId(id);
        return ResponseEntity.ok().body(service.updateLaboratory(lab));
    }

    @GetMapping("/laboratories")
    public ResponseEntity<List<Laboratory>> getLaboratories() {
        return ResponseEntity.ok().body(service.getLaboratories());
    }

    @GetMapping("/laboratories/{id}")
    public ResponseEntity<Laboratory> getProductById(@PathVariable int id) {
        return ResponseEntity.ok().body(service.getLaboratory(id));
    }

    @DeleteMapping("/laboratories/{id}")
    public ResponseEntity deleteLaboratory(@PathVariable int id) {
        return service.delete(id);
    }

}
