package com.softtek.msacademy.workareas.controller;

import com.softtek.msacademy.workareas.model.Consultory;
import com.softtek.msacademy.workareas.service.ConsultoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ConsultoryController {

    @Autowired
    private ConsultoryService service;

    @PostMapping("/consultories")
    public ResponseEntity addNewConsultory(@RequestBody Consultory consultory){
        if (service.exists(consultory.getId())) {
    		return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid record, already exists");
        }else {
        	service.createConsultory(consultory);
            return ResponseEntity.ok().body(consultory);
    	}
    }

    @PutMapping("/consultories/{id}")
    public ResponseEntity<Consultory> updateProduct(@PathVariable int id, @RequestBody Consultory consultory) {
        consultory.setId(id);
        return ResponseEntity.ok().body(service.updateConsultory(consultory));
    }

    @GetMapping("/consultories")
    public ResponseEntity<List<Consultory>> getConsultories() {
        return ResponseEntity.ok().body(service.getConsultories());
    }

    @GetMapping("/consultories/{id}")
    public ResponseEntity<Consultory> getConsultory(@PathVariable int id) {
        return ResponseEntity.ok().body(service.getConsultory(id));
    }

    @DeleteMapping("/consultories/{id}")
    public ResponseEntity deleteConsultory(@PathVariable int id) {
        return service.delete(id);
    }
}
